This is a very early version of the WP43C program for DM42. Use it at your own risk.  

Many things simply do not work! You will have to press reset quite often.  

Installation instructions:  
Copy the **WP43C.pgm** file to the DM42 flash disk over USB.
Flash the **WP43C.pgm** file just like SDKdemo.pgm (https://github.com/swissmicros/SDKdemo)
From free42: select [shift][SETUP][5][2][4][3] WP43S.pgm [ENTER][ENTER] wait [EXIT][EXIT]  

To leave the WP43S program: in the menu [f][MODES][up] select [f][f][SYSTEM] () to return to the DMCP system.

http://www.cocoon-creations.com/download/IMG_0547.JPG

