;*************************************************************
;*************************************************************
;**                                                         **
;**                    STRING - ...                         **
;**                    ... - STRING                         **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnSubtract



;==================================================================
; String - Long Integer  see in subtraction_longInteger.txt
; String - Real16        see in subtraction_real16.txt
; String - Complex16     see in subtraction_complex16.txt
; String - Time          see in subtraction_time.txt
; String - Date          see in subtraction_date.txt
;==================================================================



;=======================================
; String - String --> Error24
;=======================================
In:  SL=0 FI=0 RY=Stri:"String test " RX=Stri:"WP43S!"
Out: EC=24 FI=0 SL=0 RX=Stri:"WP43S!"



;=======================================
; String - Real16 Matrix --> Error24
;=======================================

;=======================================
; Real16 Matrix - String --> Error24
;=======================================



;=======================================
; String - Complex16 Matrix --> Error24
;=======================================

;=======================================
; Complex16 Matrix - String --> Error24
;=======================================



;=======================================
; String - Short Integer --> Error24
;=======================================
In:  SL=0 RY=Stri:"String test " RX=ShoI:"1234ABCD#16"
Out: EC=24 FI=0 SL=0 RX=ShoI:"1234ABCD#16"

;=======================================
; Short Integer - String --> Error24
;=======================================
In:  SL=0 RY=ShoI:"12540#9" RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; String - Real34 --> Error24
;=======================================
In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.34"
Out: EC=24 SL=0 RX=Re34:"-12.34"

In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.34":DEG
Out: EC=24 SL=0 RX=Re34:"-12.34":DEG

In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.345678":DMS
Out: EC=24 SL=0 RX=Re34:"-12.345678":DMS

In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.345678":GRAD
Out: EC=24 SL=0 RX=Re34:"-12.345678":GRAD

In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.345678":RAD
Out: EC=24 SL=0 RX=Re34:"-12.345678":RAD

In:  SL=0 RY=Stri:"String test " RX=Re34:"-12.345678":MULTPI
Out: EC=24 SL=0 RX=Re34:"-12.345678":MULTPI

;=======================================
; Real34 - String --> Error24
;=======================================
In:  SL=0 RY=Re34:"12540" RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"

In:  SL=0 RY=Re34:"12540":GRAD RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; String - Complex34 --> Error24
;=======================================
In:  SL=0 FI=0 RY=Stri:"String test " RX=Co34:"-12.34 i 52"
Out: EC=24 SL=0 RX=Co34:"-12.34 i 52"

;=======================================
; Complex34 - String --> Error24
;=======================================
In:  SL=0 RY=Co34:"12540 i 5" RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"
