;*************************************************************
;*************************************************************
;**                                                         **
;**                 SHORT INTEGER | | ...                   **
;**                 ... | | SHORT INTEGER                   **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnParallel



;==================================================================
; Short Integer || Long Integer      see in multiplication_longInteger.txt
; Short Integer || Real16            see in multiplication_real16.txt
; Short Integer || Complex16         see in multiplication_complex16.txt
; Short Integer || Time              see in multiplication_time.txt
; Short Integer || Date              see in multiplication_date.txt
; Short Integer || String            see in multiplication_string.txt
; Short Integer || Real16 Matrix     see in multiplication_real16Matrix.txt
; Short Integer || Complex16 Matrix  see in multiplication_complex16Matrix.txt
;==================================================================



;=======================================
; Short Integer || Short Integer --> Error24
;=======================================
In:  SL=0 FI=0 RY=ShoI:"12540#10" RX=ShoI:"10000#2"
Out: EC=24 FI=0 SL=0 RY=ShoI:"12540#10" RX=ShoI:"10000#2"



;=======================================
; Short Integer || Real34 --> Error24
;=======================================
In:  SL=0 FI=0 IM=2COMPL RY=ShoI:"45247#8" RX=Re34:"123456"
Out: EC=24 FI=0 SL=0 RY=ShoI:"45247#8" RX=Re34:"123456"

In:  AM=DEG FI=0 SL=0 NCSD=34 RY=ShoI:"2#10" RX=Re34:"123.4":DEG
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#10" RX=Re34:"123.4":DEG

In:  AM=DMS FI=0 SL=0 RY=ShoI:"10#2" RX=Re34:"123.4":DEG
Out: EC=24 FI=0 SL=0 RY=ShoI:"10#2" RX=Re34:"123.4":DEG

In:  AM=GRAD FI=0 SL=0 RY=ShoI:"2#16" RX=Re34:"123.4":DEG
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#16" RX=Re34:"123.4":DEG

In:  AM=RAD FI=0 SL=0 RY=ShoI:"2#11" RX=Re34:"123.4":DEG
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#11" RX=Re34:"123.4":DEG

In:  AM=MULTPI FI=0 SL=0 RY=ShoI:"232#4" RX=Re34:"123.4":DEG
Out: EC=24 FI=0 SL=0 RY=ShoI:"232#4" RX=Re34:"123.4":DEG

In:  AM=DEG FI=0 SL=0 RY=ShoI:"987#12" RX=Re34:"123.453099":DMS
Out: EC=24 FI=0 SL=0 RY=ShoI:"987#12" RX=Re34:"123.453099":DMS

In:  AM=DMS FI=0 SL=0 RY=ShoI:"2#3" RX=Re34:"123.453099":DMS
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#3" RX=Re34:"123.453099":DMS

In:  AM=GRAD FI=0 SL=0 RY=ShoI:"2#5" RX=Re34:"123.453099":DMS
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#5" RX=Re34:"123.453099":DMS

In:  AM=RAD FI=0 SL=0 RY=ShoI:"2#6" RX=Re34:"123.453099":DMS
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#6" RX=Re34:"123.453099":DMS

In:  AM=MULTPI FI=0 SL=0 RY=ShoI:"2#7" RX=Re34:"123.453099":DMS
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#7" RX=Re34:"123.453099":DMS

In:  AM=DEG FI=0 SL=0 RY=ShoI:"2#9" RX=Re34:"123.453099":GRAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#9" RX=Re34:"123.453099":GRAD

In:  AM=DMS FI=0 SL=0 RY=ShoI:"2#13" RX=Re34:"123.453099":GRAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#13" RX=Re34:"123.453099":GRAD

In:  AM=GRAD FI=0 SL=0 RY=ShoI:"2#14" RX=Re34:"123.453099":GRAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#14" RX=Re34:"123.453099":GRAD

In:  AM=RAD FI=0 SL=0 RY=ShoI:"2#15" RX=Re34:"123.453099":GRAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#15" RX=Re34:"123.453099":GRAD

In:  AM=MULTPI FI=0 SL=0 RY=ShoI:"2#3" RX=Re34:"123.453099":GRAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#3" RX=Re34:"123.453099":GRAD

In:  AM=DEG FI=0 SL=0 RY=ShoI:"2#4" RX=Re34:"3.453099789":RAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#4" RX=Re34:"3.453099789":RAD

In:  AM=DMS FI=0 SL=0 RY=ShoI:"2#5" RX=Re34:"3.453099789":RAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#5" RX=Re34:"3.453099789":RAD

In:  AM=GRAD FI=0 SL=0 RY=ShoI:"2#6" RX=Re34:"3.453099789":RAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#6" RX=Re34:"3.453099789":RAD

In:  AM=RAD FI=0 SL=0 RY=ShoI:"2#7" RX=Re34:"3.453099789":RAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#7" RX=Re34:"3.453099789":RAD

In:  AM=MULTPI FI=0 SL=0 RY=ShoI:"2#8" RX=Re34:"3.453099789":RAD
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#8" RX=Re34:"3.453099789":RAD

In:  AM=DEG FI=0 SL=0 RY=ShoI:"2#9" RX=Re34:"3.453099789":MULTPI
Out: EC=24 FI=0 SL=0

In:  AM=DMS FI=0 SL=0 RY=ShoI:"2#10" RX=Re34:"3.453099789":MULTPI
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#10" RX=Re34:"3.453099789":MULTPI

In:  AM=GRAD FI=0 SL=0 RY=ShoI:"2#11" RX=Re34:"3.453099789":MULTPI
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#11" RX=Re34:"3.453099789":MULTPI

In:  AM=RAD FI=0 SL=0 RY=ShoI:"2#12" RX=Re34:"3.453099789":MULTPI
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#12" RX=Re34:"3.453099789":MULTPI

In:  AM=MULTPI FI=0 SL=0 RY=ShoI:"2#13" RX=Re34:"3.453099789":MULTPI
Out: EC=24 FI=0 SL=0 RY=ShoI:"2#13" RX=Re34:"3.453099789":MULTPI

;=======================================
; Real34 || Short Integer --> Error24
;=======================================
In:  SL=0 FI=0 IM=2COMPL NCSD=34 RY=Re34:"123456" RX=ShoI:"45247#8"
Out: EC=24 FI=0 SL=0 RY=Re34:"123456" RX=ShoI:"45247#8"

In:  AM=DEG FI=0 SL=0 NCSD=34 RY=Re34:"123.4":DEG RX=ShoI:"2#10"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"2#10"

In:  AM=DMS FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"10#2"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"10#2"

In:  AM=GRAD FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"2#16"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"2#16"

In:  AM=RAD FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"2#11"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"2#11"

In:  AM=MULTPI FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"232#4"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.4":DEG RX=ShoI:"232#4"

In:  AM=DEG FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"987#12"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"987#12"

In:  AM=DMS FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#3"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#3"

In:  AM=GRAD FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#5"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#5"

In:  AM=RAD FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#6"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#6"

In:  AM=MULTPI FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#7"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":DMS RX=ShoI:"2#7"

In:  AM=DEG FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#9"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#9"

In:  AM=DMS FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#13"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#13"

In:  AM=GRAD FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#14"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#14"

In:  AM=RAD FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#15"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#15"

In:  AM=MULTPI FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#3"
Out: EC=24 FI=0 SL=0 RY=Re34:"123.453099":GRAD RX=ShoI:"2#3"

In:  AM=DEG FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#4"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#4"

In:  AM=DMS FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#5"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#5"

In:  AM=GRAD FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#6"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#6"

In:  AM=RAD FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#7"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#7"

In:  AM=MULTPI FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#8"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":RAD RX=ShoI:"2#8"

In:  AM=DEG FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#9"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#9"

In:  AM=DMS FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#10"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#10"

In:  AM=GRAD FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#11"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#11"

In:  AM=RAD FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#12"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#12"

In:  AM=MULTPI FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#13"
Out: EC=24 FI=0 SL=0 RY=Re34:"3.453099789":MULTPI RX=ShoI:"2#13"


;=======================================
; Short Integer || Complex34 --> Error24
;=======================================
In:  SL=0 FI=0 NCSD=34 RY=ShoI:"12540#10" RX=Co34:"123.456 i -6.78"
Out: EC=24 FI=0 SL=0 RY=ShoI:"12540#10" RX=Co34:"123.456 i -6.78"

;=======================================
; Complex34 || Short Integer --> Error24
;=======================================
In:  SL=0 FI=0 IM=2COMPL NCSD=34 RY=Co34:"123456 i -546" RX=ShoI:"45247#8"
Out: EC=24 FI=0 SL=0 RY=Co34:"123456 i -546" RX=ShoI:"45247#8"
