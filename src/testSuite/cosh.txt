;*************************************************************
;*************************************************************
;**                                                         **
;**                          cosh                           **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnCosh



;=======================================
; cosh(Long Integer) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"5"
Out: EC=0 FI=0 SL=1 RL=LonI:"5" RX=Re16:"74.209948524787844444106108044487714023868258589453172066091575"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"605"
Out: EC=0 FI=0 SL=1 RL=LonI:"605" RX=Re16:"2.7998293110958330698960856457790086626380084506050858348638298e262"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"-595"
Out: EC=0 FI=0 SL=1 RL=LonI:"-595" RX=Re16:"1.2711205407069717016503807475754516506872117364758988416941984e258"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"0"
Out: EC=0 FI=0 SL=1 RL=LonI:"0" RX=Re16:"1"



;=======================================
; cosh(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0"
Out: EC=0 FI=0 SL=1 RL=Re16:"0" RX=Re16:"1"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re16:"0.0001" RX=Re16:"1.000000005000000004166666668055555555803571428598985890654645"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"50"
Out: EC=0 FI=0 SL=1 RL=Re16:"50" RX=Re16:"2592352764293536232043.7266614667426924137346467294155991479176"

In:  AM=DEG SL=0 FI=0 RX=Re16:"9.999999999"
Out: EC=0 FI=0 SL=1 RL=Re16:"9.999999999" RX=Re16:"11013.23290909009027052459917141747816647470421259469785954698"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0":DEG
Out: EC=0 FI=0 SL=1 RL=Re16:"0":DEG RX=Re16:"1"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0.0001":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"0.0001":DMS RX=Re16:"1.000000005000000004166666668055555555803571428598985890654645"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"50":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"50":GRAD RX=Re16:"2592352764293536232043.7266614667426924137346467294155991479176"

In:  AM=DEG SL=0 FI=0 RX=Re16:"9.999999999":RAD
Out: EC=0 FI=0 SL=1 RL=Re16:"9.999999999":RAD RX=Re16:"11013.23290909009027052459917141747816647470421259469785954698"

; NaN
In:  SL=0 FI=0 RX=Re16:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN":RAD

In:  SL=0 FI=0 RX=Re16:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re16:"inf"
Out: EC=0 FI=0 SL=1 RL=Re16:"inf" RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf"
Out: EC=1 FI=0 SL=0 RX=Re16:"inf"

In:  SL=0 FI=0 FD=1 RX=Re16:"-inf"
Out: EC=0 FI=0 SL=1 RL=Re16:"-inf" RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf"
Out: EC=1 FI=0 SL=0 RX=Re16:"-inf"

In:  SL=0 FI=0 FD=1 RX=Re16:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re16:"inf":DEG RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re16:"-inf":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re16:"-inf":MULTPI RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf":GRAD
Out: EC=1 FI=0 SL=0 RX=Re16:"-inf":GRAD



;=======================================
; cosh(Complex16) --> Complex16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Co16:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"6.2 i -7.6" RX=Co16:"61.904278206035530628208841392918516914607105448667143436852455 i -238.46976296148906374438545158606509502378790242901267069775277"



;=======================================
; cosh(Time)
;=======================================



;=======================================
; cosh(Date)
;=======================================



;=======================================
; cosh(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; cosh(Real16 Matrix)
;=======================================



;=======================================
; cosh(Complex16 Matrix)
;=======================================



;=======================================
; cosh(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=24 FI=0 SL=0 RX=ShoI:"5#7"



;=======================================
; cosh(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0"
Out: EC=0 FI=0 SL=1 RL=Re34:"0" RX=Re34:"1"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re34:"0.0001" RX=Re34:"1.000000005000000004166666668055555555803571428598985890654645"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"50"
Out: EC=0 FI=0 SL=1 RL=Re34:"50" RX=Re34:"2592352764293536232043.7266614667426924137346467294155991479176"

In:  AM=DEG SL=0 FI=0 RX=Re34:"9.999999999"
Out: EC=0 FI=0 SL=1 RL=Re34:"9.999999999" RX=Re34:"11013.23290909009027052459917141747816647470421259469785954698"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0":DEG
Out: EC=0 FI=0 SL=1 RL=Re34:"0":DEG RX=Re34:"1"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0.0001":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"0.0001":DMS RX=Re34:"1.000000005000000004166666668055555555803571428598985890654645"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"50":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"50":GRAD RX=Re34:"2592352764293536232043.7266614667426924137346467294155991479176"

In:  AM=DEG SL=0 FI=0 RX=Re34:"9.999999999":RAD
Out: EC=0 FI=0 SL=1 RL=Re34:"9.999999999":RAD RX=Re34:"11013.23290909009027052459917141747816647470421259469785954698"

; NaN
In:  SL=0 FI=0 RX=Re34:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN":RAD

In:  SL=0 FI=0 RX=Re34:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re34:"inf"
Out: EC=0 FI=0 SL=1 RL=Re34:"inf" RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf"
Out: EC=1 FI=0 SL=0 RX=Re34:"inf"

In:  SL=0 FI=0 FD=1 RX=Re34:"-inf"
Out: EC=0 FI=0 SL=1 RL=Re34:"-inf" RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf"
Out: EC=1 FI=0 SL=0 RX=Re34:"-inf"

In:  SL=0 FI=0 FD=1 RX=Re34:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re34:"inf":DEG RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re34:"-inf":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re34:"-inf":MULTPI RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf":GRAD
Out: EC=1 FI=0 SL=0 RX=Re34:"-inf":GRAD



;=======================================
; cosh(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Co34:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"6.2 i -7.6" RX=Co34:"61.904278206035530628208841392918516914607105448667143436852455 i -238.46976296148906374438545158606509502378790242901267069775277"
