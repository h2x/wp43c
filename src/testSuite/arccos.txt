;*************************************************************
;*************************************************************
;**                                                         **
;**                        arccos                           **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnArccos



;=======================================
; arccos(Long Integer) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"-2"
Out: EC=1 FI=0 SL=0 RX=LonI:"-2"

In:  AM=DEG SL=0 FI=0 RX=LonI:"-1"
Out: EC=0 FI=0 SL=1 RL=LonI:"-1" RX=Re16:"180":DEG

In:  AM=GRAD SL=0 FI=0 RX=LonI:"0"
Out: EC=0 FI=0 SL=1 RL=LonI:"0" RX=Re16:"100":GRAD

In:  AM=RAD SL=0 FI=0 RX=LonI:"1"
Out: EC=0 FI=0 SL=1 RL=LonI:"1" RX=Re16:"0":RAD

In:  AM=DEG SL=0 FI=0 RX=LonI:"2"
Out: EC=1 FI=0 SL=0 RX=LonI:"2"

In:  AM=DEG SL=0 FI=1 RX=LonI:"-2"
Out: EC=0 FI=1 SL=1 RL=LonI:"-2" RX=Co16:"3.1415926535897932384626433832795028841971693993751058209749446 i 1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=1 RX=LonI:"2"
Out: EC=0 FI=1 SL=1 RL=LonI:"2" RX=Co16:"0 i -1.3169578969248167086250463473079684440269819714675164797684723"



;=======================================
; arccos(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"-2"
Out: EC=1 FI=0 SL=0 RX=Re16:"-2"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1"
Out: EC=0 FI=0 SL=1 RL=Re16:"-1" RX=Re16:"180":DEG

In:  AM=GRAD SL=0 FI=0 RX=Re16:"0"
Out: EC=0 FI=0 SL=1 RL=Re16:"0" RX=Re16:"100":GRAD

In:  AM=RAD SL=0 FI=0 RX=Re16:"1"
Out: EC=0 FI=0 SL=1 RL=Re16:"1" RX=Re16:"0":RAD

In:  AM=DEG SL=0 FI=0 RX=Re16:"2"
Out: EC=1 FI=0 SL=0 RX=Re16:"2"

In:  AM=DEG SL=0 FI=1 RX=Re16:"-2"
Out: EC=0 FI=1 SL=1 RL=Re16:"-2" RX=Co16:"3.1415926535897932384626433832795028841971693993751058209749446 i 1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=1 RX=Re16:"2"
Out: EC=0 FI=1 SL=1 RL=Re16:"2" RX=Co16:"0 i -1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"-2":DEG
Out: EC=1 FI=0 SL=0 RX=Re16:"-2":DEG

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1":RAD
Out: EC=0 FI=0 SL=1 RL=Re16:"-1":RAD RX=Re16:"180":DEG

In:  AM=GRAD SL=0 FI=0 RX=Re16:"0":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re16:"0":MULTPI RX=Re16:"100":GRAD

In:  AM=RAD SL=0 FI=0 RX=Re16:"1":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"1":GRAD RX=Re16:"0":RAD

In:  AM=DEG SL=0 FI=0 RX=Re16:"2":DMS
Out: EC=1 FI=0 SL=0 RX=Re16:"2":DMS

In:  AM=DEG SL=0 FI=1 RX=Re16:"-2":DEG
Out: EC=0 FI=1 SL=1 RL=Re16:"-2":DEG RX=Co16:"3.1415926535897932384626433832795028841971693993751058209749446 i 1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=1 RX=Re16:"2":RAD
Out: EC=0 FI=1 SL=1 RL=Re16:"2":RAD RX=Co16:"0 i -1.3169578969248167086250463473079684440269819714675164797684723"



;=======================================
; arccos(Complex16) --> Complex16
;=======================================
In:  AM=DEG SL=0 FI=1 RX=Co16:"-2 i -3"
Out: EC=0 FI=1 SL=1 RL=Co16:"-2 i -3" RX=Co16:"2.1414491111159960199416055713254210922813879447835930470177456 i +1.9833870299165354323470769028940395650142483029093453561252674"



;=======================================
; arccos(Time)
;=======================================



;=======================================
; arccos(Date)
;=======================================



;=======================================
; arccos(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; arccos(Real16 Matrix)
;=======================================



;=======================================
; arccos(Complex16 Matrix)
;=======================================



;=======================================
; arccos(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=24 FI=0 SL=0 RX=ShoI:"5#7"



;=======================================
; arccos(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"-2"
Out: EC=1 FI=0 SL=0 RX=Re34:"-2"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1"
Out: EC=0 FI=0 SL=1 RL=Re34:"-1" RX=Re34:"180":DEG

In:  AM=GRAD SL=0 FI=0 RX=Re34:"0"
Out: EC=0 FI=0 SL=1 RL=Re34:"0" RX=Re34:"100":GRAD

In:  AM=RAD SL=0 FI=0 RX=Re34:"1"
Out: EC=0 FI=0 SL=1 RL=Re34:"1" RX=Re34:"0":RAD

In:  AM=DEG SL=0 FI=0 RX=Re34:"2"
Out: EC=1 FI=0 SL=0 RX=Re34:"2"

In:  AM=DEG SL=0 FI=1 RX=Re34:"-2"
Out: EC=0 FI=1 SL=1 RL=Re34:"-2" RX=Co34:"3.1415926535897932384626433832795028841971693993751058209749446 i 1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=1 RX=Re34:"2"
Out: EC=0 FI=1 SL=1 RL=Re34:"2" RX=Co34:"0 i -1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"-2":DEG
Out: EC=1 FI=0 SL=0 RX=Re34:"-2":DEG

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1":RAD
Out: EC=0 FI=0 SL=1 RL=Re34:"-1":RAD RX=Re34:"180":DEG

In:  AM=GRAD SL=0 FI=0 RX=Re34:"0":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re34:"0":MULTPI RX=Re34:"100":GRAD

In:  AM=RAD SL=0 FI=0 RX=Re34:"1":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"1":GRAD RX=Re34:"0":RAD

In:  AM=DEG SL=0 FI=0 RX=Re34:"2":DMS
Out: EC=1 FI=0 SL=0 RX=Re34:"2":DMS

In:  AM=DEG SL=0 FI=1 RX=Re34:"-2":DEG
Out: EC=0 FI=1 SL=1 RL=Re34:"-2":DEG RX=Co34:"3.1415926535897932384626433832795028841971693993751058209749446 i 1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=1 RX=Re34:"2":RAD
Out: EC=0 FI=1 SL=1 RL=Re34:"2":RAD RX=Co34:"0 i -1.3169578969248167086250463473079684440269819714675164797684723"


;=======================================
; arccos(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=1 RX=Co34:"-2 i -3"
Out: EC=0 FI=1 SL=1 RL=Co34:"-2 i -3" RX=Co34:"2.1414491111159960199416055713254210922813879447835930470177456 i +1.9833870299165354323470769028940395650142483029093453561252674"
