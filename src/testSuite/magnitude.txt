;*************************************************************
;*************************************************************
;**                                                         **
;**                       magnitude                         **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnMagnitude



;=======================================
; magnitude(Long Integer) --> Long Integer
;=======================================
In:  AM=DEG SL=0 FI=0 RX=LonI:"5"
Out: EC=0 FI=0 SL=1 RL=LonI:"5" RX=LonI:"5"

In:  AM=DEG SL=0 FI=0 RX=LonI:"-5"
Out: EC=0 FI=0 SL=1 RL=LonI:"-5" RX=LonI:"5"



;=======================================
; magnitude(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re16:"0.0001" RX=Re16:"0.0001"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"50"
Out: EC=0 FI=0 SL=1 RL=Re16:"50" RX=Re16:"50"

In:  AM=DEG SL=0 FI=0 RX=Re16:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re16:"89.99999" RX=Re16:"89.99999"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-123.456"
Out: EC=0 FI=0 SL=1 RL=Re16:"-123.456" RX=Re16:"123.456"

In:  SL=0 FI=0 NCSD=16 RX=Re16:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"5.32564":DMS RX=Re16:"5.32564"

In:  SL=0 FI=0 RX=Re16:"-5.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"-5.32564":GRAD RX=Re16:"5.32564"

; NaN
In:  SL=0 FI=0 RX=Re16:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN":RAD

In:  SL=0 FI=0 RX=Re16:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re16:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re16:"inf":DEG RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf":MULTPI
Out: EC=4 FI=0 SL=0 RX=Re16:"inf":MULTPI

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf":RAD
Out: EC=4 FI=0 SL=0 RX=Re16:"-inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re16:"inf"
Out: EC=0 FI=0 SL=1 RL=Re16:"inf" RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf"
Out: EC=4 FI=0 SL=0 RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf"
Out: EC=4 FI=0 SL=0 RX=Re16:"-inf"



;=======================================
; magnitude(Complex16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Co16:"6.2 i -7.6"
Out: EC=0 FI=0 SL=1 RL=Co16:"6.2 i -7.6" RX=Re16:"9.8081598681913826154186097680415343588218978912800102069046454"



;=======================================
; magnitude(Time)
;=======================================



;=======================================
; magnitude(Date)
;=======================================



;=======================================
; magnitude(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; magnitude(Real16 Matrix)
;=======================================



;=======================================
; magnitude(Complex16 Matrix)
;=======================================



;=======================================
; magnitude(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=0 FI=0 SL=1 RL=ShoI:"5#7" RX=ShoI:"5#7"



;=======================================
; magnitude(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re34:"0.0001" RX=Re34:"0.0001"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"50"
Out: EC=0 FI=0 SL=1 RL=Re34:"50" RX=Re34:"50"

In:  AM=DEG SL=0 FI=0 RX=Re34:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re34:"89.99999" RX=Re34:"89.99999"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-123.456"
Out: EC=0 FI=0 SL=1 RL=Re34:"-123.456" RX=Re34:"123.456"

In:  SL=0 FI=0 NCSD=34 RX=Re34:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"5.32564":DMS RX=Re34:"5.32564"

In:  SL=0 FI=0 RX=Re34:"-5.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"-5.32564":GRAD RX=Re34:"5.32564"

; NaN
In:  SL=0 FI=0 RX=Re34:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN":RAD

In:  SL=0 FI=0 RX=Re34:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re34:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re34:"inf":DEG RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf":MULTPI
Out: EC=4 FI=0 SL=0 RX=Re34:"inf":MULTPI

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf":RAD
Out: EC=4 FI=0 SL=0 RX=Re34:"-inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re34:"inf"
Out: EC=0 FI=0 SL=1 RL=Re34:"inf" RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf"
Out: EC=4 FI=0 SL=0 RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf"
Out: EC=4 FI=0 SL=0 RX=Re34:"-inf"



;=======================================
; magnitude(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Co34:"6.2 i -7.6"
Out: EC=0 FI=0 SL=1 RL=Co34:"6.2 i -7.6" RX=Re34:"9.8081598681913826154186097680415343588218978912800102069046454"
