;*************************************************************
;*************************************************************
;**                                                         **
;**                   COMPLEX34 | | ...                     **
;**                   ... | | COMPLEX34                     **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnParallel



;==================================================================
; Complex34 || Long Integer      see in multiplication_longInteger.txt
; Complex34 || Real16            see in multiplication_real16.txt
; Complex34 || Complex16         see in multiplication_complex16.txt
; Complex34 || Time              see in multiplication_time.txt
; Complex34 || Date              see in multiplication_date.txt
; Complex34 || String            see in multiplication_string.txt
; Complex34 || Real16 Matrix     see in multiplication_real16Matrix.txt
; Complex34 || Complex16 Matrix  see in multiplication_complex16Matrix.txt
; Complex34 || Short Integer     see in multiplication_shortInteger.txt
; Complex34 || Real34            see in multiplication_real34.txt
;==================================================================



;=======================================
; Complex34 || Complex34 --> Complex34
;=======================================
In:  SL=0 FI=0 NCSD=34 RY=Co34:"0 i 0" RX=Co34:"0 i 0"
Out: EC=0 FI=1 SL=1 RL=Co34:"0 i 0" RX=Co34:"0 i 0"

In:  SL=0 FI=0 NCSD=16 RY=Co34:"1 i 1" RX=Co34:"2 i -1"
Out: EC=0 FI=1 SL=1 RL=Co34:"2 i -1" RX=Co34:"1 i +0.33333333333333333333333333333333333333333333333333333333333333"
