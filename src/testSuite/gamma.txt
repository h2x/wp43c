;*************************************************************
;*************************************************************
;**                                                         **
;**                         gamma                           **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnGamma



;=======================================
; gamma(Long Integer) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 RX=LonI:"46"
Out: EC=0 FI=0 SL=1 RL=LonI:"46" RX=Re16:"119622220865480194561963161495657715064383733760000000000"

In:  AM=DEG SL=0 FI=0 RX=LonI:"0"
Out: EC=1 FI=0 SL=0 RX=LonI:"0"

In:  AM=DEG SL=0 FI=0 RX=LonI:"-5"
Out: EC=1 FI=0 SL=0 RX=LonI:"-5"



;=======================================
; gamma(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"46"
Out: EC=0 FI=0 SL=1 RL=Re16:"46" RX=Re16:"119622220865480194561963161495657715064383733760000000000"

In:  AM=DEG SL=0 FI=0 RX=Re16:"12.36"
Out: EC=0 FI=0 SL=1 RL=Re16:"12.36" RX=Re16:"96711173.804679283764059550367208933471847423437567588886346282"

In:  AM=DEG SL=0 FI=0 RX=Re16:"54.32"
Out: EC=0 FI=0 SL=1 RL=Re16:"54.32" RX=Re16:"1.5290239239705496551132494690996954526783542339649495989632494e+70"

In:  AM=DEG SL=0 FI=0 RX=Re16:"0"
Out: EC=1 FI=0 SL=0 RX=Re16:"0"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"4.1"
Out: EC=0 FI=0 SL=1 RL=Re16:"4.1" RX=Re16:"6.8126228630166788679690500676397545100854693541418870153581056"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"-1.1"
Out: EC=0 FI=0 SL=1 RL=Re16:"-1.1" RX=Re16:"9.714806382902903226339139415404370710634898046008847082267006"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-2.1"
Out: EC=0 FI=0 SL=1 RL=Re16:"-2.1" RX=Re16:"-4.6260982775728110601614949597163670050642371647661176582223839"

In:  SL=0 FI=0 NCSD=16 RX=Re16:"6.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"6.32564":DMS RX=Re16:"211.13598684879991116840112854302517382023008855382603631608281"

In:  SL=0 FI=0 RX=Re16:"-4.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"-4.32564":GRAD RX=Re16:"-0.092823884274579407865808232851777690972248476783570824903801507"



;=======================================
; gamma(Complex16) --> Complex16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Co16:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"6.2 i -7.6" RX=Co16:"-1.6724066357653852921174882842654853012776480990051891929281362 i -2.1471425625020440567811763759197971050483081850017064173111305"

In:  AM=DEG SL=0 FI=0 RX=Co16:"6.2 i 7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"6.2 i 7.6" RX=Co16:"-1.6724066357653852921174882842654853012776480990051891929281362 i 2.1471425625020440567811763759197971050483081850017064173111305"

In:  AM=DEG SL=0 FI=0 RX=Co16:"-6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"-6.2 i -7.6" RX=Co16:"6.093866764725242894523990537978077455529755219944798767973533e-12 i -8.001596227759724236614650692262884769794185678880715436824347e-12"

In:  AM=DEG SL=0 FI=0 RX=Co16:"-6.2 i 7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"-6.2 i 7.6" RX=Co16:"6.0938667647252428945239905379780774555297552199447987679735327e-12 i 8.001596227759724236614650692262884769794185678880715436824347e-12"



;=======================================
; gamma(Time)
;=======================================



;=======================================
; gamma(Date)
;=======================================



;=======================================
; gamma(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; gamma(Real16 Matrix)
;=======================================



;=======================================
; gamma(Complex16 Matrix)
;=======================================



;=======================================
; gamma(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#8"
Out: EC=24 FI=0 SL=0 RX=ShoI:"5#8"



;=======================================
; gamma(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"46"
Out: EC=0 FI=0 SL=1 RL=Re34:"46" RX=Re34:"119622220865480194561963161495657715064383733760000000000"

In:  AM=DEG SL=0 FI=0 RX=Re34:"12.36"
Out: EC=0 FI=0 SL=1 RL=Re34:"12.36" RX=Re34:"96711173.804679283764059550367208933471847423437567588886346282"

In:  AM=DEG SL=0 FI=0 RX=Re34:"54.32"
Out: EC=0 FI=0 SL=1 RL=Re34:"54.32" RX=Re34:"1.5290239239705496551132494690996954526783542339649495989632494e+70"

In:  AM=DEG SL=0 FI=0 RX=Re34:"0"
Out: EC=1 FI=0 SL=0 RX=Re34:"0"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"4.1"
Out: EC=0 FI=0 SL=1 RL=Re34:"4.1" RX=Re34:"6.8126228630166788679690500676397545100854693541418870153581056"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"-1.1"
Out: EC=0 FI=0 SL=1 RL=Re16:"-1.1" RX=Re16:"9.714806382902903226339139415404370710634898046008847082267006"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-2.1"
Out: EC=0 FI=0 SL=1 RL=Re34:"-2.1" RX=Re34:"-4.6260982775728110601614949597163670050642371647661176582223839"

In:  SL=0 FI=0 NCSD=34 RX=Re34:"6.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"6.32564":DMS RX=Re34:"211.13598684879991116840112854302517382023008855382603631608281"

In:  SL=0 FI=0 RX=Re34:"-4.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"-4.32564":GRAD RX=Re34:"-0.092823884274579407865808232851777690972248476783570824903801507"



;=======================================
; gamma(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Co34:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"6.2 i -7.6" RX=Co34:"-1.6724066357653852921174882842654853012776480990051891929281362 i -2.1471425625020440567811763759197971050483081850017064173111305"

In:  AM=DEG SL=0 FI=0 RX=Co34:"6.2 i 7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"6.2 i 7.6" RX=Co34:"-1.6724066357653852921174882842654853012776480990051891929281362 i 2.1471425625020440567811763759197971050483081850017064173111305"

In:  AM=DEG SL=0 FI=0 RX=Co34:"-6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"-6.2 i -7.6" RX=Co34:"6.093866764725242894523990537978077455529755219944798767973533e-12 i -8.001596227759724236614650692262884769794185678880715436824347e-12"

In:  AM=DEG SL=0 FI=0 RX=Co34:"-6.2 i 7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"-6.2 i 7.6" RX=Co34:"6.0938667647252428945239905379780774555297552199447987679735327e-12 i 8.001596227759724236614650692262884769794185678880715436824347e-12"
