;*************************************************************
;*************************************************************
;**                                                         **
;**                        arccosh                          **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnArccosh

;TODO arccosh(-0.5) and arccosh(0.5)

;=======================================
; arccosh(Long Integer) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"-2"
Out: EC=1 FI=0 SL=0 RX=LonI:"-2"

In:  AM=DEG SL=0 FI=1 RX=LonI:"-2"
Out: EC=0 FI=1 SL=1 RL=LonI:"-2" RX=Co16:"-1.3169578969248167086250463473079684440269819714675164797684723 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"-1"
Out: EC=1 FI=0 SL=0 RX=LonI:"-1"

In:  AM=DEG SL=0 FI=1 RX=LonI:"-1"
Out: EC=0 FI=1 SL=1 RL=LonI:"-1" RX=Co16:"0 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"0"
Out: EC=1 FI=0 SL=0 RX=LonI:"0"

In:  AM=DEG SL=0 FI=1 RX=LonI:"0"
Out: EC=0 FI=1 SL=1 RL=LonI:"0" RX=Co16:"0 i 1.5707963267948966192313216916397514420985846996875529104874723"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"1"
Out: EC=0 FI=0 SL=1 RL=LonI:"1" RX=Re16:"0"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=LonI:"2"
Out: EC=0 FI=0 SL=1 RL=LonI:"2" RX=Re16:"1.3169578969248167086250463473079684440269819714675164797684723"



;=======================================
; arccosh(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"-2"
Out: EC=1 FI=0 SL=0 RX=Re16:"-2"

In:  AM=DEG SL=0 FI=1 RX=Re16:"-2"
Out: EC=0 FI=1 SL=1 RL=Re16:"-2" RX=Co16:"-1.3169578969248167086250463473079684440269819714675164797684723 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1.5"
Out: EC=1 FI=0 SL=0 RX=Re16:"-1.5"

In:  AM=DEG SL=0 FI=1 RX=Re16:"-1.5"
Out: EC=0 FI=1 SL=1 RL=Re16:"-1.5" RX=Co16:"-0.96242365011920689499551782684873684627036866877132103932203634 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1"
Out: EC=1 FI=0 SL=0 RX=Re16:"-1"

In:  AM=DEG SL=0 FI=1 RX=Re16:"-1"
Out: EC=0 FI=1 SL=1 RL=Re16:"-1" RX=Co16:"0 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-0.5"
Out: EC=1 FI=0 SL=0 RX=Re16:"-0.5"

;In:  AM=DEG SL=0 FI=1 RX=Re16:"-0.5"
;Out: EC=0 FI=1 SL=1 RL=Re16:"-0.5" RX=Co16:"0 i 2.0943951023931954923084289221863352561314462662500705473166297"

In:  AM=DEG SL=0 FI=0 RX=Re16:"0"
Out: EC=1 FI=0 SL=0 RX=Re16:"0"

In:  AM=DEG SL=0 FI=1 RX=Re16:"0"
Out: EC=0 FI=1 SL=1 RL=Re16:"0" RX=Co16:"0 i 1.5707963267948966192313216916397514420985846996875529104874723"

In:  AM=DEG SL=0 FI=0 RX=Re16:"0.5"
Out: EC=1 FI=0 SL=0 RX=Re16:"0.5"

;In:  AM=DEG SL=0 FI=1 RX=Re16:"0.5"
;Out: EC=0 FI=1 SL=1 RL=Re16:"0.5" RX=Co16:"0 i 1.0471975511965977461542144610931676280657231331250352736583149"

In:  AM=DEG SL=0 FI=0 RX=Re16:"1"
Out: EC=0 FI=0 SL=1 RL=Re16:"1" RX=Re16:"0"

In:  AM=DEG SL=0 FI=0 RX=Re16:"1.000000000000001"
Out: EC=0 FI=0 SL=1 RL=Re16:"1.000000000000001" RX=Re16:"4.4721359549995790201403510874976869219014481727656714366310018e-8"

In:  AM=DEG SL=0 FI=0 RX=Re16:"1.5"
Out: EC=0 FI=0 SL=1 RL=Re16:"1.5" RX=Re16:"0.96242365011920689499551782684873684627036866877132103932203634"

In:  AM=DEG SL=0 FI=0 RX=Re16:"2"
Out: EC=0 FI=0 SL=1 RL=Re16:"2" RX=Re16:"1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 RX=Re16:"2.5"
Out: EC=0 FI=0 SL=1 RL=Re16:"2.5" RX=Re16:"1.566799236972411078664056862580483493862082351092658863932946"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"-2":DEG
Out: EC=1 FI=0 SL=0 RX=Re16:"-2":DEG

In:  AM=DEG SL=0 FI=1 RX=Re16:"-2":DMS
Out: EC=0 FI=1 SL=1 RL=Re16:"-2":DMS RX=Co16:"-1.3169578969248167086250463473079684440269819714675164797684723 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1.5":GRAD
Out: EC=1 FI=0 SL=0 RX=Re16:"-1.5":GRAD

In:  AM=DEG SL=0 FI=1 RX=Re16:"-1.5":RAD
Out: EC=0 FI=1 SL=1 RL=Re16:"-1.5":RAD RX=Co16:"-0.96242365011920689499551782684873684627036866877132103932203634 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-1":MULTPI
Out: EC=1 FI=0 SL=0 RX=Re16:"-1":MULTPI

In:  AM=DEG SL=0 FI=1 RX=Re16:"-1":DEG
Out: EC=0 FI=1 SL=1 RL=Re16:"-1":DEG RX=Co16:"0 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re16:"-0.5":DMS
Out: EC=1 FI=0 SL=0 RX=Re16:"-0.5":DMS

;In:  AM=DEG SL=0 FI=1 RX=Re16:"-0.5":GRAD
;Out: EC=0 FI=1 SL=1 RL=Re16:"-0.5":GRAD RX=Co16:"0 i 2.0943951023931954923084289221863352561314462662500705473166297"

In:  AM=DEG SL=0 FI=0 RX=Re16:"0":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"0":RAD

In:  AM=DEG SL=0 FI=1 RX=Re16:"0":MULTPI
Out: EC=0 FI=1 SL=1 RL=Re16:"0":MULTPI RX=Co16:"0 i 1.5707963267948966192313216916397514420985846996875529104874723"

In:  AM=DEG SL=0 FI=0 RX=Re16:"0.5":DEG
Out: EC=1 FI=0 SL=0 RX=Re16:"0.5":DEG

;In:  AM=DEG SL=0 FI=1 RX=Re16:"0.5":DMS
;Out: EC=0 FI=1 SL=1 RL=Re16:"0.5":DMS RX=Co16:"0 i 1.0471975511965977461542144610931676280657231331250352736583149"

In:  AM=DEG SL=0 FI=0 RX=Re16:"1":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"1":GRAD RX=Re16:"0"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"1.000000000000001":RAD
Out: EC=0 FI=0 SL=1 RL=Re16:"1.000000000000001":RAD RX=Re16:"4.4721359549995790201403510874976869219014481727656714366310018e-8"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"1.5":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re16:"1.5":MULTPI RX=Re16:"0.96242365011920689499551782684873684627036866877132103932203634"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"2":DEG
Out: EC=0 FI=0 SL=1 RL=Re16:"2":DEG RX=Re16:"1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"2.5":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"2.5":DMS RX=Re16:"1.566799236972411078664056862580483493862082351092658863932946"



;=======================================
; arccosh(Complex16) --> Complex16
;=======================================
In:  AM=DEG SL=0 FI=1 RX=Co16:"-2 i -3"
Out: EC=0 FI=1 SL=1 RL=Co16:"-2 i -3" RX=Co16:"-1.9833870299165354323470769028940395650142483029093453561252674 i 2.1414491111159960199416055713254210922813879447835930470177456"



;=======================================
; arccosh(Time)
;=======================================



;=======================================
; arccosh(Date)
;=======================================



;=======================================
; arccosh(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; arccosh(Real16 Matrix)
;=======================================



;=======================================
; arccosh(Complex16 Matrix)
;=======================================



;=======================================
; arccosh(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=24 FI=0 SL=0 RX=ShoI:"5#7"



;=======================================
; arccosh(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"-2"
Out: EC=1 FI=0 SL=0 RX=Re34:"-2"

In:  AM=DEG SL=0 FI=1 RX=Re34:"-2"
Out: EC=0 FI=1 SL=1 RL=Re34:"-2" RX=Co34:"-1.3169578969248167086250463473079684440269819714675164797684723 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1.5"
Out: EC=1 FI=0 SL=0 RX=Re34:"-1.5"

In:  AM=DEG SL=0 FI=1 RX=Re34:"-1.5"
Out: EC=0 FI=1 SL=1 RL=Re34:"-1.5" RX=Co34:"-0.96242365011920689499551782684873684627036866877132103932203634 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1"
Out: EC=1 FI=0 SL=0 RX=Re34:"-1"

In:  AM=DEG SL=0 FI=1 RX=Re34:"-1"
Out: EC=0 FI=1 SL=1 RL=Re34:"-1" RX=Co34:"0 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-0.5"
Out: EC=1 FI=0 SL=0 RX=Re34:"-0.5"

;In:  AM=DEG SL=0 FI=1 RX=Re34:"-0.5"
;Out: EC=0 FI=1 SL=1 RL=Re34:"-0.5" RX=Co34:"0 i 2.0943951023931954923084289221863352561314462662500705473166297"

In:  AM=DEG SL=0 FI=0 RX=Re34:"0"
Out: EC=1 FI=0 SL=0 RX=Re34:"0"

In:  AM=DEG SL=0 FI=1 RX=Re34:"0"
Out: EC=0 FI=1 SL=1 RL=Re34:"0" RX=Co34:"0 i 1.5707963267948966192313216916397514420985846996875529104874723"

In:  AM=DEG SL=0 FI=0 RX=Re34:"0.5"
Out: EC=1 FI=0 SL=0 RX=Re34:"0.5"

;In:  AM=DEG SL=0 FI=1 RX=Re34:"0.5"
;Out: EC=0 FI=1 SL=1 RL=Re34:"0.5" RX=Co34:"0 i 1.0471975511965977461542144610931676280657231331250352736583149"

In:  AM=DEG SL=0 FI=0 RX=Re34:"1"
Out: EC=0 FI=0 SL=1 RL=Re34:"1" RX=Re34:"0"

In:  AM=DEG SL=0 FI=0 RX=Re34:"1.000000000000000000000000000000001"
Out: EC=0 FI=0 SL=1 RL=Re34:"1.000000000000000000000000000000001" RX=Re34:"4.4721359549995793928183473374625520982032404692581020470128497e-17"

In:  AM=DEG SL=0 FI=0 RX=Re34:"1.5"
Out: EC=0 FI=0 SL=1 RL=Re34:"1.5" RX=Re34:"0.96242365011920689499551782684873684627036866877132103932203634"

In:  AM=DEG SL=0 FI=0 RX=Re34:"2"
Out: EC=0 FI=0 SL=1 RL=Re34:"2" RX=Re34:"1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 RX=Re34:"2.5"
Out: EC=0 FI=0 SL=1 RL=Re34:"2.5" RX=Re34:"1.566799236972411078664056862580483493862082351092658863932946"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"-2":DEG
Out: EC=1 FI=0 SL=0 RX=Re34:"-2":DEG

In:  AM=DEG SL=0 FI=1 RX=Re34:"-2":DMS
Out: EC=0 FI=1 SL=1 RL=Re34:"-2":DMS RX=Co34:"-1.3169578969248167086250463473079684440269819714675164797684723 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1.5":GRAD
Out: EC=1 FI=0 SL=0 RX=Re34:"-1.5":GRAD

In:  AM=DEG SL=0 FI=1 RX=Re34:"-1.5":RAD
Out: EC=0 FI=1 SL=1 RL=Re34:"-1.5":RAD RX=Co34:"-0.96242365011920689499551782684873684627036866877132103932203634 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-1":MULTPI
Out: EC=1 FI=0 SL=0 RX=Re34:"-1":MULTPI

In:  AM=DEG SL=0 FI=1 RX=Re34:"-1":DEG
Out: EC=0 FI=1 SL=1 RL=Re34:"-1":DEG RX=Co34:"0 i 3.1415926535897932384626433832795028841971693993751058209749446"

In:  AM=DEG SL=0 FI=0 RX=Re34:"-0.5":DMS
Out: EC=1 FI=0 SL=0 RX=Re34:"-0.5":DMS

;In:  AM=DEG SL=0 FI=1 RX=Re34:"-0.5":GRAD
;Out: EC=0 FI=1 SL=1 RL=Re34:"-0.5":GRAD RX=Co34:"0 i 2.0943951023931954923084289221863352561314462662500705473166297"

In:  AM=DEG SL=0 FI=0 RX=Re34:"0":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"0":RAD

In:  AM=DEG SL=0 FI=1 RX=Re34:"0":MULTPI
Out: EC=0 FI=1 SL=1 RL=Re34:"0":MULTPI RX=Co34:"0 i 1.5707963267948966192313216916397514420985846996875529104874723"

In:  AM=DEG SL=0 FI=0 RX=Re34:"0.5":DEG
Out: EC=1 FI=0 SL=0 RX=Re34:"0.5":DEG

;In:  AM=DEG SL=0 FI=1 RX=Re34:"0.5":DMS
;Out: EC=0 FI=1 SL=1 RL=Re34:"0.5":DMS RX=Co34:"0 i 1.0471975511965977461542144610931676280657231331250352736583149"

In:  AM=DEG SL=0 FI=0 RX=Re34:"1":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"1":GRAD RX=Re34:"0"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"1.000000000000000000000000000000001":RAD
Out: EC=0 FI=0 SL=1 RL=Re34:"1.000000000000000000000000000000001":RAD RX=Re34:"4.4721359549995793928183473374625520982032404692581020470128497e-17"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"1.5":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re34:"1.5":MULTPI RX=Re34:"0.96242365011920689499551782684873684627036866877132103932203634"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"2":DEG
Out: EC=0 FI=0 SL=1 RL=Re34:"2":DEG RX=Re34:"1.3169578969248167086250463473079684440269819714675164797684723"

In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"2.5":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"2.5":DMS RX=Re34:"1.566799236972411078664056862580483493862082351092658863932946"


;=======================================
; arccosh(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=1 RX=Co34:"-2 i -3"
Out: EC=0 FI=1 SL=1 RL=Co34:"-2 i -3" RX=Co34:"-1.9833870299165354323470769028940395650142483029093453561252674 i 2.1414491111159960199416055713254210922813879447835930470177456"
