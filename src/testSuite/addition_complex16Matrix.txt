;*************************************************************
;*************************************************************
;**                                                         **
;**                COMPLEX16 MATRIX + ...                   **
;**                ... + COMPLEX16 MATRIX                   **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnAdd



;==================================================================
; Complex16 Matrix + Long Integer   see in addition_longInteger.txt
; Complex16 Matrix + Real16         see in addition_real16.txt
; Complex16 Matrix + Complex16      see in addition_complex16.txt
; Complex16 Matrix + Time           see in addition_time.txt
; Complex16 Matrix + Date           see in addition_date.txt
; Complex16 Matrix + String         see in addition_string.txt
; Complex16 Matrix + Real16 Matrix  see in addition_real16Matrix.txt
;==================================================================



;=======================================
; Complex16 Matrix + Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix + Short Integer
;=======================================

;=======================================
; Short Integer + Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix + Real34
;=======================================

;=======================================
; Real34 + Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix + Complex34
;=======================================

;=======================================
; Complex34 + Complex16 Matrix
;=======================================
