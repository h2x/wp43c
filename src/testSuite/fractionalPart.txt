;*************************************************************
;*************************************************************
;**                                                         **
;**                    fractional part                      **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnFp



;=======================================
; fp(Long Integer) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 RX=LonI:"5"
Out: EC=0 FI=0 SL=1 RL=LonI:"5" RX=LonI:"0"



;=======================================
; fp(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re16:"0.0001" RX=Re16:"0.0001"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"50"
Out: EC=0 FI=0 SL=1 RL=Re16:"50" RX=Re16:"0"

In:  AM=DEG SL=0 FI=0 RX=Re16:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re16:"89.99999" RX=Re16:"0.99999"

In:  SL=0 FI=0 NCSD=16 RX=Re16:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"5.32564":DMS RX=Re16:"0.32564":DMS

In:  SL=0 FI=0 RX=Re16:"-5.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re16:"-5.32564":GRAD RX=Re16:"-0.32564":GRAD

; NaN
In:  SL=0 FI=0 RX=Re16:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN":RAD

; NaN
In:  SL=0 FI=0 RX=Re16:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN"



;=======================================
; fp(Complex16) --> Error
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Co16:"6.2 i -7.6"
Out: EC=24 FI=0 SL=0 RX=Co16:"6.2 i -7.6"



;=======================================
; fp(Time)
;=======================================



;=======================================
; fp(Date)
;=======================================



;=======================================
; fp(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; fp(Real16 Matrix)
;=======================================



;=======================================
; fp(Complex16 Matrix)
;=======================================



;=======================================
; fp(Short Integer) --> Short Integer
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=0 FI=0 SL=1 RX=ShoI:"0#7"



;=======================================
; fp(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0.0001"
Out: EC=0 FI=0 SL=1 RL=Re34:"0.0001" RX=Re34:"0.0001"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"50"
Out: EC=0 FI=0 SL=1 RL=Re34:"50" RX=Re34:"0"

In:  AM=DEG SL=0 FI=0 RX=Re34:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re34:"89.99999" RX=Re34:"0.99999"

In:  SL=0 FI=0 NCSD=34 RX=Re34:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"5.32564":DMS RX=Re34:"0.32564":DMS

In:  SL=0 FI=0 RX=Re34:"-5.32564":GRAD
Out: EC=0 FI=0 SL=1 RL=Re34:"-5.32564":GRAD RX=Re34:"-0.32564":GRAD

; NaN
In:  SL=0 FI=0 RX=Re34:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN":RAD

; NaN
In:  SL=0 FI=0 RX=Re34:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN"



;=======================================
; fp(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Co34:"6.2 i -7.6"
Out: EC=24 FI=0 SL=0 RX=Co34:"6.2 i -7.6"
