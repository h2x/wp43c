;*************************************************************
;*************************************************************
;**                                                         **
;**                      cube root                          **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnCubeRoot



;=======================================
; curt(Long Integer) --> Long Integer or Real16 or Complex16
;=======================================
In:  AM=DEG SL=0 FI=0 RX=LonI:"0"
Out: EC=0 FI=0 SL=1 RL=LonI:"0" RX=LonI:"0"

In:  AM=DEG SL=0 FI=0 RX=LonI:"5"
Out: EC=0 FI=0 SL=1 RL=LonI:"5" RX=Re16:"1.7099759466766969893531088725438601098680551105430549243828617"

In:  AM=DEG SL=0 FI=0 RX=LonI:"4096"
Out: EC=0 FI=0 SL=1 RL=LonI:"4096" RX=LonI:"16"

;In:  AM=DEG SL=0 FI=0 RX=LonI:"-5"
;Out: EC=0 FI=0 SL=1 RL=LonI:"-5" RX=Re16:"-1.7099759466766969893531088725438601098680551105430549243828617"

In:  AM=DEG SL=0 FI=0 RX=LonI:"-4096"
Out: EC=0 FI=0 SL=1 RL=LonI:"-4096" RX=LonI:"-16"



;=======================================
; curt(Real16) --> Real16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Re16:"0.000001"
Out: EC=0 FI=0 SL=1 RL=Re16:"0.000001" RX=Re16:"0.01"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"50"
Out: EC=0 FI=0 SL=1 RL=Re16:"50" RX=Re16:"3.6840314986403866057798228335798072219172581174381826692564615"

In:  AM=DEG SL=0 FI=0 RX=Re16:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re16:"89.99999" RX=Re16:"4.4814045805792049852242031069194557138979834132204433509894446"

In:  AM=GRAD SL=0 FI=0 RX=Re16:"-50"
Out: EC=0 FI=0 SL=1 RL=Re16:"-50" RX=Re16:"-3.6840314986403866057798228335798072219172581174381826692564615"

In:  SL=0 FI=0 NCSD=16 RX=Re16:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"5.32564":DMS RX=Re16:"1.7463204319901609356922737268876921175934116112721107102426736"

In:  SL=0 FI=0 NCSD=16 RX=Re16:"-5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re16:"-5.32564":DMS RX=Re16:"-1.7463204319901609356922737268876921175934116112721107102426736"

; NaN
In:  SL=0 FI=0 RX=Re16:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN":RAD

In:  SL=0 FI=0 RX=Re16:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re16:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re16:"inf"
Out: EC=0 FI=0 SL=1 RL=Re16:"inf" RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf"
Out: EC=1 FI=0 SL=0 RX=Re16:"inf"

In:  SL=0 FI=0 FD=1 RX=Re16:"-inf"
Out: EC=0 FI=0 SL=1 RL=Re16:"-inf" RX=Re16:"-inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf"
Out: EC=1 FI=0 SL=0 RX=Re16:"-inf"

In:  SL=0 FI=0 FD=1 RX=Re16:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re16:"inf":DEG RX=Re16:"inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"inf":RAD
Out: EC=1 FI=0 SL=0 RX=Re16:"inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re16:"-inf":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re16:"-inf":MULTPI RX=Re16:"-inf"

In:  SL=0 FI=0 FD=0 RX=Re16:"-inf":GRAD
Out: EC=1 FI=0 SL=0 RX=Re16:"-inf":GRAD



;=======================================
; curt(Complex16) --> Complex16
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=16 RX=Co16:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co16:"6.2 i -7.6" RX=Co16:"2.0477890059563993605878674711353032501056843427634424499720687 i -0.62337365477424462191731015529133991023246328104058833011841894"



;=======================================
; curt(Time)
;=======================================



;=======================================
; curt(Date)
;=======================================



;=======================================
; curt(String) --> Error24
;=======================================
In:  SL=0 RX=Stri:"String test"
Out: EC=24 SL=0 RX=Stri:"String test"



;=======================================
; curt(Real16 Matrix)
;=======================================



;=======================================
; curt(Complex16 Matrix)
;=======================================



;=======================================
; curt(Short Integer) --> Error24
;=======================================
In:  SL=0 FI=0 RX=ShoI:"5#7"
Out: EC=0 FI=0 SL=1 RL=ShoI:"5#7" RX=ShoI:"1#7"

In:  SL=0 FI=0 RX=ShoI:"625#10"
Out: EC=0 FI=0 SL=1 RL=ShoI:"625#10" RX=ShoI:"8#10"

In:  SL=0 FI=0 RX=ShoI:"-625#10"
Out: EC=0 FI=0 SL=1 RL=ShoI:"-625#10" RX=ShoI:"-8#10"



;=======================================
; curt(Real34) --> Real34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Re34:"0.000001"
Out: EC=0 FI=0 SL=1 RL=Re34:"0.000001" RX=Re34:"0.01"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"50"
Out: EC=0 FI=0 SL=1 RL=Re34:"50" RX=Re34:"3.6840314986403866057798228335798072219172581174381826692564615"

In:  AM=DEG SL=0 FI=0 RX=Re34:"89.99999"
Out: EC=0 FI=0 SL=1 RL=Re34:"89.99999" RX=Re34:"4.4814045805792049852242031069194557138979834132204433509894446"

In:  AM=GRAD SL=0 FI=0 RX=Re34:"-50"
Out: EC=0 FI=0 SL=1 RL=Re34:"-50" RX=Re34:"-3.6840314986403866057798228335798072219172581174381826692564615"

In:  SL=0 FI=0 NCSD=34 RX=Re34:"5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"5.32564":DMS RX=Re34:"1.7463204319901609356922737268876921175934116112721107102426736"

In:  SL=0 FI=0 NCSD=34 RX=Re34:"-5.32564":DMS
Out: EC=0 FI=0 SL=1 RL=Re34:"-5.32564":DMS RX=Re34:"-1.7463204319901609356922737268876921175934116112721107102426736"

; NaN
In:  SL=0 FI=0 RX=Re34:"NaN":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN":RAD

In:  SL=0 FI=0 RX=Re34:"NaN"
Out: EC=1 FI=0 SL=0 RX=Re34:"NaN"

; Infinity
In:  SL=0 FI=0 FD=1 RX=Re34:"inf"
Out: EC=0 FI=0 SL=1 RL=Re34:"inf" RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf"
Out: EC=1 FI=0 SL=0 RX=Re34:"inf"

In:  SL=0 FI=0 FD=1 RX=Re34:"-inf"
Out: EC=0 FI=0 SL=1 RL=Re34:"-inf" RX=Re34:"-inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf"
Out: EC=1 FI=0 SL=0 RX=Re34:"-inf"

In:  SL=0 FI=0 FD=1 RX=Re34:"inf":DEG
Out: EC=0 FI=0 SL=1 RL=Re34:"inf":DEG RX=Re34:"inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"inf":RAD
Out: EC=1 FI=0 SL=0 RX=Re34:"inf":RAD

In:  SL=0 FI=0 FD=1 RX=Re34:"-inf":MULTPI
Out: EC=0 FI=0 SL=1 RL=Re34:"-inf":MULTPI RX=Re34:"-inf"

In:  SL=0 FI=0 FD=0 RX=Re34:"-inf":GRAD
Out: EC=1 FI=0 SL=0 RX=Re34:"-inf":GRAD



;=======================================
; curt(Complex34) --> Complex34
;=======================================
In:  AM=DEG SL=0 FI=0 NCSD=34 RX=Co34:"6.2 i -7.6"
Out: EC=0 FI=1 SL=1 RL=Co34:"6.2 i -7.6" RX=Co34:"2.0477890059563993605878674711353032501056843427634424499720687 i -0.62337365477424462191731015529133991023246328104058833011841894"
