;*************************************************************
;*************************************************************
;**                                                         **
;**                COMPLEX16 MATRIX / ...                   **
;**                ... / COMPLEX16 MATRIX                   **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnDivide



;==================================================================
; Complex16 Matrix / Long Integer   see in division_longInteger.txt
; Complex16 Matrix / Real16         see in division_real16.txt
; Complex16 Matrix / Complex16      see in division_complex16.txt
; Complex16 Matrix / Time           see in division_time.txt
; Complex16 Matrix / Date           see in division_date.txt
; Complex16 Matrix / String         see in division_string.txt
; Complex16 Matrix / Real16 Matrix  see in division_real16Matrix.txt
;==================================================================



;=======================================
; Complex16 Matrix / Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix / Short Integer
;=======================================

;=======================================
; Short Integer / Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix / Real34
;=======================================

;=======================================
; Real34 / Complex16 Matrix
;=======================================



;=======================================
; Complex16 Matrix / Complex34
;=======================================

;=======================================
; Complex34 / Complex16 Matrix
;=======================================
