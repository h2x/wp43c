;*************************************************************
;*************************************************************
;**                                                         **
;**                    COMPLEX34 + ...                      **
;**                    ... + COMPLEX34                      **
;**                                                         **
;*************************************************************
;*************************************************************
In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnAdd



;==================================================================
; Complex34 + Long Integer      see in addition_longInteger.txt
; Complex34 + Real16            see in addition_real16.txt
; Complex34 + Complex16         see in addition_complex16.txt
; Complex34 + Time              see in addition_time.txt
; Complex34 + Date              see in addition_date.txt
; Complex34 + String            see in addition_string.txt
; Complex34 + Real16 Matrix     see in addition_real16Matrix.txt
; Complex34 + Complex16 Matrix  see in addition_complex16Matrix.txt
; Complex34 + Short Integer     see in addition_shortInteger.txt
; Complex34 + Real34            see in addition_real34.txt
;==================================================================



;=======================================
; Complex34 + Complex34 --> Complex34
;=======================================
In:  SL=0 FI=0 NCSD=34 RY=Co34:"123.456 i -6.78" RX=Co34:"-12540 i 7.541235"
Out: EC=0 FI=1 SL=1 RL=Co34:"-12540 i 7.541235" RX=Co34:"-12416.544 i 0.761235"

In:  SL=0 FI=0 RY=Co34:"NaN i -6.78" RX=Co34:"-12540 i 7.541235"
Out: EC=1 FI=0 SL=0 RL=Co34:"-12540 i 7.541235"

In:  SL=1 FI=0 RY=Co34:"123.456 i NaN" RX=Co34:"-12540 i 7.541235"
Out: EC=1 FI=0 SL=1 RL=Co34:"-12540 i 7.541235"

In:  SL=0 FI=0 RY=Co34:"123.456 i -6.78" RX=Co34:"NaN i 7.541235"
Out: EC=1 FI=0 SL=0 RY=Co34:"123.456 i -6.78" RX=Co34:"NaN i 7.541235"

In:  SL=1 FI=0 RY=Co34:"123.456 i -6.78" RX=Co34:"-12540 i NaN"
Out: EC=1 FI=0 SL=1 RY=Co34:"123.456 i -6.78" RX=Co34:"-12540 i NaN"
