# Windows 64 bit binaries  
This binary is provided for your convenience.  

You must install the latest GTK3+ libraries. If you do not already have them: download and install the latest release of the gtk3-runtime (not gtk2-runtime) for win64 here https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer/releases

Install or re-install the WP43S_StandardFont.ttf font. The WP43S_NumericFont.ttf font isn't needed.  
Copy wp43s.exe in a new or pre-existing directory.  
Copy dm42l.png, dm42lshort.png and wp43s_pre.css from the root of this gitlab project to the same directory  
If there is a backup.bin file in your already existing directory: delete it  

Run wp43s.exe and enjoy.
